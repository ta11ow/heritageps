There are a number of PowerShell and other ''scripts'' that are available for configuring computers and installing necessary software more or less automatically. 

!!! Make backups of any files ''before'' modifying them.

!!! Test //all// possible use cases before overwriting a file.

!! Usage

Note that these //must// be run via the following shell command (in an elevated Command Prompt window). To run an elevated Command Prompt, you must have either local or domain admin permissions.

Click Start, type in //cmd//, and select Run As Different User from the right-click menu on //cmd.exe//. Enter local or domain admin account and password.

```
powershell -executionpolicy bypass -file "SCRIPT-PATH"
```

Where SCRIPT-PATH is the path to the script you want to run. If you wish to remove this necessity and enable the scripts to be run directly, you will need to modify the [[Group Policy]] in order to change the default PowerShell Execution Policy.
It is recommended that you enable running of remote-signed scripts, and have all your scripts signed by an on-network root CA.

The currently available scripts are:

* [[Init.ps1]]
* [[App-Autodesk.ps1]]
* [[App-Showers.ps1]]
* [[Install-WMF5.1.ps1]]
* [[MapNetworkDrivesWin7.bat|MapNetworkDrives]]
* [[MapNetworkDrivesWin10.ps1|MapNetworkDrives]]

There are also some supporting scripts defining functions used in the primary scripts, located in the ''Functions\'' folder.