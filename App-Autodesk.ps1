. "\\hct-byn-fs01\Apps\Scripts\Functions\Install-Program.ps1"
. "\\hct-byn-fs01\Apps\Scripts\Functions\File-Operations.ps1"

Save-Transcript -Folder "\\hct-byn-fs01\Apps\Scripts\Logs"

#Working directory
$opsdir = "\\HCT-BYN-FS01\Documents\Heritage-Specialties\DWF\"

Install-Program "Autodesk" "$opsdir\SetupDesignReview.exe"