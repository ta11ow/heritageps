[CmdletBinding()]
Param()
Begin
{
    . "\\hct-byn-fs01\Apps\Scripts\Functions\Misc.ps1"
    . "\\hct-byn-fs01\Apps\Scripts\Functions\File-Operations.ps1"

    # Drive locations
    $Company = "\\HCT-BYN-FS01\Company"
    $Docs = "\\HCT-BYN-FS01\Documents"
}
Process
{
    New-NetworkDrive G $Company
    New-NetworkDrive H $Docs
}
End
{
    Start-Sleep 8
}