﻿[CmdletBinding()]
Param
(
    [System.Uri]$sourceURL
)
Begin
{
    if (!$PSBoundParameters.ContainsKey('sourceURL'))
    {
        $sourceURL = [System.Uri]"http://www.dictionary.com/wordoftheday/"
    }
    $n = [Environment]::NewLine
}
Process
{
    # The code below is specifically designed for this webpage. 
    # Adjust the array index of Images[] to get the specific
    # image(s) you want to use.

    $imgSRC = (Invoke-WebRequest -Uri $sourceURL).Images[1].src

    Write-Verbose "Image src string:$($env:NEWLINE)$imgSRC$($env:NEWLINE)"

    # On this particular page, the URL is actually fed through
    # a redirect. This is annoying, so we'll just strip the
    # URL down to the final target and parse it.

    $URLSplit = $imgSRC.Split(';')[2]
    $encodedURL = $URLSplit.Substring(4, $URLSplit.Length - 4)

    $TargetURL = [System.Uri]::UnescapeDataString($encodedURL)

    Write-Verbose "Final image URL: $TargetURL"

    $Path = "$($env:HOMESHARE)\Wallpapers\Daily"
    if (!(Test-Path $Path))
    {
        New-Item -ItemType directory $Path | Write-Output
    }

    $Image = $TargetURL | Split-Path -Leaf
    $SavePath = "$Path\$Image"

    if (Test-Path $SavePath)
    {
        Remove-Item $SavePath | Write-Output
    }

    try {
        Invoke-WebRequest -Uri $TargetURL -OutFile $SavePath
    }
    catch {
        
    }
}
End
{
    # Actually setting the wallpaper seems to work only intermittently.
    # Multiple methods have failed to achieve the desired result... somehow.
    
    # Instead, we'll just open the downloaded image file with the default program

    Invoke-Item $SavePath
}