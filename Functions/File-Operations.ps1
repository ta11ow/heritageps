﻿. '\\hct-byn-fs01\apps\scripts\functions\Process-Error.ps1'
function New-Folder 
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$True)]
        [ValidateScript({Test-Path -IsValid $_})]
        [string]$Path
    )

    try 
    {
        New-Item -Path $Path -ItemType Directory -ErrorAction Stop
    }
    catch [System.IO.IOException]
    {
        if ($_.CategoryInfo -ne "ResourceExists") {
            Process-Error $_
        }
        else {
            Write-Warning "Could not create folder '$($_.TargetObject)' because it already exists."
        }
    }
    catch 
    {
        Process-Error $_
    }
}

function Set-Permission 
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$True)]
        [string]$Object, 

        [Parameter(Mandatory=$True)]
        [string]$Permission, 

        [Parameter(Mandatory=$True)]
        [ValidateScript({Test-Path $_})]
        [string]$Path, 

        [switch]$Cascade, 
        [switch]$Deny
    )
    
    switch ($Deny) 
    {
        $False 
        {
            switch ($Cascade) 
            {
                $True 
                {
                    $rule=New-Object System.Security.AccessControl.FileSystemAccessRule($Object,$Permission,"ContainerInherit,ObjectInherit","None","Allow")
                }
                $False 
                {
                    $rule=New-Object System.Security.AccessControl.FileSystemAccessRule($Object,$Permission,"Allow")
                }
            }
        }
        $True 
        {
            switch ($Cascade) 
            {
                $True 
                {
                    $rule=New-Object System.Security.AccessControl.FileSystemAccessRule($Object,$Permission,"ContainerInherit,ObjectInherit","None","Deny")
                }
                $False 
                {
                    $rule=New-Object System.Security.AccessControl.FileSystemAccessRule($Object,$Permission,"Deny")
                }
            }
        }
    }
    
    try
    {
        #Remove possibly conflicting access rules
        $acl = Get-Acl $Path 
        $ace = $acl.Access | Where-Object { $Path.IdentityReference -eq $Object }
        if ($ace -ne $null) 
        {
            $acl.RemoveAccessRule($ace) | Write-Verbose
        }
   
        #Set new access rule
        $acl.SetAccessRule($rule) | Write-Verbose
        Set-ACL -Path $Path -ACLObject $acl -PassThru
    }
    catch
    {
        Process-Error $_
    }
}

function New-Shortcut 
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$True)]
        [ValidateScript({Test-Path -IsValid $_})]
        [string]$Path, 
        
        [Parameter(Mandatory=$True)]
        [ValidateScript({Test-Path $_})]
        [string]$Target, 

        [string]$Icon=""
    )
    try 
    {
        $WshShell = New-Object -ComObject WScript.Shell
        $Shortcut = $WshShell.CreateShortcut($Path)
        $Shortcut.TargetPath = $Target
        if($Icon -ne "") 
        {
            $shortcut.IconLocation = "$Icon"
        }
        $Shortcut.Save() | Write-Verbose
    }
    catch 
    {
        Process-Error $_
    }
}

function Invoke-Download 
{
    [CmdletBinding()]
    Param 
    (
        [Parameter(Mandatory=$True)]
        [Uri]$URL,

        [Parameter(Mandatory=$True)]
        [ValidateScript({Test-Path -IsValid $_})]
        [string]$OutFile,

        [switch]$Force,
        [int32]$Method,

        [Parameter(Mandatory=$False)]
        [ValidateScript({Test-Path -IsValid $_})]
        [string]$LogFile
    )
    
    $Backup = $False
    
    Write-Output "Downloading file from `"$URL`" to `"$OutFile`""
    
    $OutFolder = $OutFile | Split-Path -Parent
    if (-not (Test-Path $OutFolder)) {
        New-Folder $OutFolder
    }
    
    if ((Test-Path $OutFile)) 
    {
        if ($Force) 
        {
            $Backup = $True
            $BAKPath = "$($OutFile).BAK"
            if (Test-Path $BAKPath) 
            {
                Remove-Item $BAKPath
            }
            Move-Item $OutFile $BAKPath | Write-Verbose
        }
        else 
        {
            Write-Warning "The specified destination already contains a file with the specified name. Aborting."
            Write-Warning "If you wish to overwrite existing files, please use the '-Force' flag."
            return $False
        }
    }

    $Completed = $False
    $Attempts = 0

    while (-not $Completed) 
    {
        try 
        {
            $start_time = Get-Date
            if (($Attempts -eq 1) -and ($Method))
            {
                if ($Method -gt 2)
                {
                    $Method = $Method % 3
                }
                $method_num = $Method
            }
            else
            {
                $method_num = $Attempts % 3
            }

            # Due to how $Attempts is implemented, the first attempt will be case 1 (System.Net.WebClient.DownloadFile), then iterating through if it fails.
            # Method 2 is the fastest, but BITS will only use idle bandwidth, creating an indefinite stall until the network is free of,
            # for example, Windows Updates and any other downloads. As this is intended to be used on fresh systems primarily, BITS is likely
            # to be occupied. For other uses, the method can be specified using the $Method parameter. Method 0 is slowest, so should be tried last of all.
            switch ($method_num) {
                0 {
                    $method_name = "Invoke-WebRequest"
                    Write-Output "Attempting download with Invoke-WebRequest..."
                    Invoke-WebRequest -Uri $URL -OutFile $OutFile | Write-Output
                    break
                }
                1 { 
                    $method_name = "System.Net.WebClient"
                    (New-Object System.Net.WebClient).DownloadFile($URL, $OutFile) | Write-Output
                    break
                }
                2 {
                    $method_name = "BITS"
                    Import-Module BitsTransfer
                    Start-BitsTransfer -Source $URL -Destination $OutFile | Write-Output
                    break
                }
            }
            $Completed = $True # If no exceptions were thrown, assume the download succeeded and proceed.
        }
        catch 
        {
            if ($PSBoundParameters.ContainsKey('LogFile')) {
                Process-Error -ErrorRecord $_ -LogFile $LogFile
            }
            else {
                Process-Error -ErrorRecord $_
            }
            Write-Host "Retrying..."
        }
        finally 
        {
        
            $Attempts++
            if ($Completed) {
                Write-Host "Download from '$URL' completed successfully." -ForegroundColor Green
                Write-Host "Time taken: " -NoNewLine 
                Write-Host "$((Get-Date).Subtract($start_time).Seconds) second(s)." -ForegroundColor Yellow
            }
        }
        if (($Attempt -gt 5) -and !$Completed) 
        {
            Write-Warning "Aborted download after $Attempts attempts."
            if ($Backup) 
            {
                Write-Output "A backup of an earlier downloaded file exists and can be used."
                Move-Item $BAKPath $OutFile | Write-Verbose
                $Completed = $True
            }
            else 
            {
                Write-Warning "There is no backup present for this file at the specified location, and the download failed. Aborting."
                break
            }
        }
        # If the download completed, exit the loop. Otherwise, loop back and try something else!
    }

    # Return whether there is a file available to use.
    return $Completed
}