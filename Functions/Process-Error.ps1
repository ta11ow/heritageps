﻿function Process-Error {
    [CmdletBinding()]
    Param(
        [Parameter(
            Mandatory = $True,
            ValueFromPipeline = $True
        )]
        [System.Management.Automation.ErrorRecord]$ErrorRecord,

        [ValidateScript({Test-Path -IsValid -Path $_})]
        [string]$LogFile
    )
    Begin {
        $Timestamp = Get-Date -Format "[yyyy.MM.dd](hh:mm:ss)"
        $Exception = $ErrorRecord.Exception
        $Data = New-Object PSObject -Property @{
            'Timestamp' = $Timestamp
            'Error Type' = $Exception.GetType()
            'Message' = $Exception.Message
            'Problem Code' = $Exception.CommandName
            'Details' = $Exception.ErrorDetails
            'StackTrace' = $Exception.StackTrace
        }
    }
    Process {
        if ($PSBoundParameters.ContainsKey('LogFile')) {
            $LogFolder = $LogFile | Split-Path -Parent
            if (-not (Test-Path $LogFolder)) {
                New-Item -ItemType Directory -Path $LogFolder
            }
            if (-not (Test-Path $LogFile)) {
                New-Item $LogFile
            }
        }
    }
    End {
        if ($PSBoundParameters.ContainsKey('LogFile')) {
            Add-Content -Path $LogFile -Value $Data
            Write-Output "A logfile of all errors was saved to '$LogFile'"
        }
        else {
            Write-Error "A $($Exception.GetType()) exception was encountered."
            Write-Error "Error details:"
            Write-Error $Data
        }
    }
}