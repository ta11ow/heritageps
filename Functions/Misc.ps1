#Maps a network drive (win10 only) and displays the result
function New-NetworkDrive 
{
    Param
    (
        [string]$Drive,
        [string]$Path
    )
    
    #Only create drives if they're not already mapped
    If (!((Get-PSDrive [a-z]) -Match $Drive)) 
    {
    	New-PSDrive -Name $Drive -PSProvider FileSystem -Root $Path -Persist | `
    	Format-Table Name, DisplayRoot, Root -AutoSize
    }
}

function Register-DLL
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$True)]
        [ValidateScript({ Test-Path $_ })]
        [string]$File, 

        [switch]$x64
    )
    
    $Name=$File.split('\')[-1]

    $SysWOW64 = "$($env:SystemRoot)\SysWOW64"
    $System32 = "$($env:SystemRoot)\System32"

    $Params = @{
        NoNewWindow = $True
        Wait = $True
        PassThru = $True
    }
    
    switch($x64) 
    {
        $False 
        {
            switch (Get-OSArch) 
            {
                "64-bit" 
                {
                    if ((Test-Path "$SysWOW64\$Name") -eq $false)
                    {
                        Copy-Item $File "$SysWOW64\" -Force
                        $Params.Add('FilePath',"$SysWoW64\regsvr32.exe")
                        $Params.Add('ArgumentList',"/s '$SysWOW64\$Name'")
                    }
                }
                default 
                {
                    if ((Test-Path "$System32\$Name") -eq $False)
                    { 
                        Copy-Item $File "$System32\" -Force
                        $Params.Add('FilePath',"$System32\regsvr32.exe")
                        $Params.Add('ArgumentsList',"/s '$System32\$Name'")
                    }
                }
            }
        }
        $True 
        {
            if ((Test-Path "$System32\$Name") -eq $false)
            {
                Copy-Item $File "$System32\" -Force
                $Params.Add('FilePath',"$System32\regsvr32.exe")
                $Params.Add('ArgumentsList',"/s '$System32\$Name'")
            }
        }
    }
    $Proc = Start-Process @Params
    
    Write-Output $Proc.StandardOutput
}

function Get-OSArch 
{
    return (Get-WmiObject Win32_OperatingSystem).OSArchitecture
}

function Get-PSVersion 
{
    return $PSVersionTable.PSVersion
}