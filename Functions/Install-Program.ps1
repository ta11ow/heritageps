﻿# Installs an .exe, .msp, or .msi packaged program, or executes a .ps1 powershell script
# To use, dot-source the script, and then call the Install-Program function

function Install-Program 
{
    [CmdletBinding()]
    Param 
    (
        [string]$Name,
        [Parameter(Mandatory=$True)]
        [string]$Path,
        [string]$Arguments = " "
    )
        
    $Type=$Path.split('.')[-1]

    if (!$PSBoundParameters.ContainsKey('Name')) {
        $Name = $Path | Split-Path -Leaf
    }
    
    $Parameters = @{
        NoNewWindow = $True
        Wait = $True
        PassThru = $True
    }
    
    $LogFile = "$($env:TEMP)\$(Get-Date -Format "yyyyMMdd hh-mm") $($Name).log"
    $Logging = $False

    switch ($Type)
    {
        "msi" 
        {
            $Logging = $True
            Write-Verbose "Using .msi installation sequence."
            $Parameters.Add('FilePath','msiexec.exe')
            $Parameters.Add('ArgumentList',"/I `"$Path`" $Arguments /Log `"$LogFile`"")
            break
        }
        "msp" 
        {
            Write-Verbose "Using .msp installation sequence."
            $Parameters.Add('FilePath','msiexec.exe')
            $Parameters.Add('ArgumentList', "/p `"$Path`" $Arguments")
            break
        }
        "msu" 
        {
            Write-Verbose "Using .msu installation sequence."
            $Parameters.Add('FilePath','wusa.exe')
            $Parameters.Add('ArgumentList',"`"$Path`" $Arguments")
            break
        }
        "exe" 
        {
            Write-Verbose "Using direct installation sequence."
            $Parameters.Add('FilePath',$Path)
            $Parameters.Add('ArgumentList',$Arguments)
            break
        }
        "ps1" 
        {
            Write-Verbose "Running PowerShell script."
            $Parameters.Add('FilePath','powershell.exe')
            $Parameters.Add('ArgumentList',"-ExecutionPolicy bypass -File `"$Path`" $Arguments")
            break
        }
        default 
        {
            Write-Error "The package was not of a recognised filetype."
            Write-Warning "Please try again with an .exe, .msi, .msu, or .msp file, or update the Functions.ps1 script accordingly."
            return
        }
    }

    $Process = Start-Process @Parameters

    if ($Logging -and $PSCmdlet.MyInvocation.BoundParameters.ContainsKey('Verbose')) {
        $Content = Get-Content $LogFile
        Write-Verbose $Content
    }

    if ($Process.ExitCode -ne 0)
    {
        Write-Error "The process exited in an unusual manner. The exit code given was $($Process.ExitCode)."
        Write-Warning "The install of $Name did not complete successfully."
    }
    else
    {
        Write-Output "Successfully installed $Name."
    }
}