﻿<#
.NAME
    Init

.SYNOPSIS
    This is a script which is capable of automatically installing most of our default programs.
        
.DESCRIPTION
    The script will, by default, install the following applications: 
     
     - ECP and the supporting MS Access frameworks
     - LSeven's Labtech remote-access agent
     - LogMeIn for Heritage remote access (confer with Glen for access)
     - Google Chrome
     - 7zip
     - Dropbox
     - Foxit Reader
     - Microsoft Teams

    You can specify a single set of programs to be installed, or excluded, from the
    specific instance.

.PARAMETERS
    -Only
        Only installs the specified set of applications. Accepted string values are: ECP,
        Remote, or Stock.
                
        Required?                   false
        Position?                   0
        Default value        
        Accept pipeline input?      false
        Accept wildcard characters? false

    -Except
        Installs everything except the specified set of applications. Accepted string values
        are: ECP, Remote, or Stock.
                
        Required?                   false
        Position?                   0
        Default value        
        Accept pipeline input?      false
        Accept wildcard characters? false

.EXAMPLE
    .\Init.ps1 -Only ECP

.INPUTS
    None. You cannot pipe objects to Init.

.OUTPUTS
    Init outputs its progress throughout the installation to stdout. Additional output is available through -Verbose.
#>
[CmdletBinding(DefaultParameterSetName = 'Default')]
Param
(
    [Parameter(
        ParameterSetName = 'Only',
        Mandatory = $True
    )]
    [ValidateSet('Stock', 'ECP', 'Remote')]
    [string]$Only,
    [Parameter(
        ParameterSetName = 'Except',
        Mandatory = $True
    )]
    [ValidateSet('Stock', 'ECP', 'Remote')]
    [string]$Except
)
Begin 
{ 
    . "\\hct-byn-fs01\Apps\Scripts\Functions\Install-Program.ps1"
    . "\\hct-byn-fs01\Apps\Scripts\Functions\File-Operations.ps1"
    . "\\hct-byn-fs01\Apps\Scripts\Functions\Misc.ps1"

    $LogFile = "$($env:TEMP)\$(Get-Date -Format 'yyyyMMdd-hhmm')-Init.log"

    if ($PSCmdlet.MyInvocation.BoundParameters["Debug"].IsPresent) 
    {
         $DebugPreference="Continue"
         Write-Debug "Debug Output activated"
    }
    else 
    {
         $DebugPreference="SilentlyContinue"
    }
    if ($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent) 
    {
         $VerbosePreference="Continue"
         Write-Verbose "Using Verbose output"
    }
    else 
    {
         $VerbosePreference="SilentlyContinue"
    }

    function Download-Package {
        [CmdletBinding()]
        Param(
            [Parameter(Mandatory=$True)]
            [string]$URL,
            [Parameter(Mandatory=$True)]
            [string]$OutFile
        )
        
        #Download the package from the web source.
        $Success = Invoke-Download $URL $OutFile -Force
    
        if ($Success) 
        {
            Write-Verbose "The file was successfully downloaded to '$OutFile'"
            return $True
        }
        else 
        {
            Write-Error "The specified file could not be retrieved for $PkgName. It will not be installed."
            return $False
        }
    }

    function Stage-Install {
        [CmdletBinding()]
        Param
        (
            [Parameter(Mandatory=$True)]
            [System.Collections.ArrayList]$PackageTable
        )

        $PackageTable | Foreach-Object {
            if (!$_.ContainsKey('Arguments')) {
                $_.Add('Arguments', ' ')
            }

            Write-Verbose "Installing $($_['Name']) from '$($_['Path'])'"
            Write-Verbose "Using arguments '$($_['Arguments'])'"
            Install-Program -Name $_['Name'] -Path $_['Path'] -Arguments $_['Arguments'] | 
                Write-Host -ForegroundColor Green
        }
    }

    function Config-ECP 
    {
        #--------ECP Post-Install Configuration--------#

        Write-Output "Performing post-installation ECP Configuration..."

        $ECPFolder = "$iHeritage\Current Version"

        $path = "C:\Program Files\ECP"
    
        Write-Verbose "Copying ECP workspace files to '$path'..."

        If(!(test-path $path))
        {
	        New-Item -ItemType Directory -Force -Path $path |
                Write-Verbose
        }
        try 
        {
            Copy-Item "$ECPFolder\ECP7.ade" $path |
                Write-Verbose

            Copy-Item "$iHeritage\ecp-launch-current7.vbs" $path |
                Write-Verbose

            Copy-Item "$iHeritage\PDFCreator*.dll" $path |
                Write-Verbose

            Copy-Item "$iHeritage\VBTablet.dll" $path |
                Write-Verbose

            Copy-Item "$iHeritage\wiaaut.dll" $path |
                Write-Verbose

            Copy-Item "$iHeritage\ecp.ico" $path |
                Write-Verbose
        }
        catch
        {
            Process-Error -ErrorRecord $_ -LogFile $LogFile
            Return
        } 

        # Create a shortcut to the VBS script used to run ECP
        Write-Verbose "Creating ECP shortcut on the desktop..."
        New-Shortcut "C:\Users\Public\Desktop\ECP.lnk" "$path\ecp-launch-current7.vbs" "$path\ecp.ico"

        # Test if system is 32 bit; if so, make a symlink so the ECP script works OK.
        $x86Path = "C:\Program Files (x86)"
        If(!(test-path $x86Path))
        {
	        New-Item -ItemType SymbolicLink -Name $x86Path -target "C:\Program Files" | 
                Write-Verbose
        }

        #Set permissions such that all domain users can access and modify the local ECP database
        Set-Permission "HCT\Domain Users" "FullControl" $path -Cascade | Write-Verbose

        #Register required DLLs for ECP
        Register-DLL "$path\wiaaut.dll"

        #Launch ECP so that the script thingy can be preconfigured
        Invoke-Item "$path\ecp-launch-current7.vbs"

        Write-Output "ECP Configuration completed."
    }

    $OpsDir = "\\HCT-BYN-FS01\Apps"
    $iHeritage = "\\HCT-BYN-FS01\Documents\History\iHeritage"

    #Set package files, package names, and command line switches.

    $Packages = New-Object System.Collections.ArrayList
    [Void]$Packages.Add(@{Set = 'Stock'; Name = "Foxit Reader"; Path = "$OpsDir\FoxitReader831.msi"; Arguments = "/qn"})
    [Void]$Packages.Add(@{Set = 'Stock'; Name = "Microsoft Teams"; Path = "\\hct-byn-fs01\Company\Teams\Install.ps1"; Arguments = "-SourcePath `"\\hct-byn-fs01\Company\Teams`""})
    [Void]$Packages.Add(@{Set = 'Stock'; Name = "Windows Management Framework 5.1"; Path = "$OpsDir\WMF 5.1\Install-WMF5.1.ps1"; Arguments = "-AcceptEULA"})
    [Void]$Packages.Add(@{Set = 'Stock'; Name = "7zip"; Path = "$OpsDir\7zip\7z1700-x64.msi"; Arguments = "/qn"})
    [Void]$Packages.Add(@{Set = 'Stock'; Name = "Google Chrome"; Path = "$OpsDir\Chrome\Chrome_Win64.msi"; Arguments = "/qn"})
    [Void]$Packages.Add(@{Set = 'Remote'; Name = "LogMeIn"; Path = "$OpsDir\LogMeIn_Install_HCT_MainOffice.msi"; Arguments = "/qn"})
    [Void]$Packages.Add(@{Set = 'ECP'; Name = "Access Runtime"; Path = "$iHeritage\Install\accessRT\2003\ACCESSRT.MSI"; Arguments = "/qn"})
    [Void]$Packages.Add(@{Set = 'ECP'; Name = "Access 2003 SP3"; Path = "$iHeritage\Install\Office 2003 Service Pack 3\Office2003SP3-KB923618-FullFile-ENU.exe"})
    [Void]$Packages.Add(@{Set = 'ECP'; Name = "Access Patches"; Path = "$iHeritage\Install\Office 2003 Security Update\office2003-KB2726929-FullFile-ENU.exe"})
    [Void]$Packages.Add(@{Set = 'ECP'; Name = "Access Hotfix"; Path = "$iHeritage\Install\Hotfix\MSACCESS.msp"; Arguments = 'REINSTALL="ALL" REINSTALLMODE=omus /qn'})
    [Void]$Packages.Add(@{Set = 'ECP'; Name = "PDF Creator Pilot"; Path = "$iHeritage\Install\PDFPilot\pdflib.msi"; Arguments = '/qn'})
        # Add additional locally-accessible packages here. Set = Stock | Remote | ECP (same as params)
    
    $WebPackages = New-Object System.Collections.ArrayList
    [Void]$WebPackages.Add(@{Set = "Remote"; Name = "LSeven Labtech"; URL = "http://labtech.lseven.com/Labtech/Deployment.aspx"; Path = "$OpsDir\Labtech\AgentInstall.exe"})
    [Void]$WebPackages.Add(@{Set = "Stock"; Name = "Dropbox"; URL = "http://dropbox.com/download"; Path = "$OpsDir\Dropbox\Dropbox.exe"; Arguments = "/S"})
        # Add additional web-hosted packages here. Set = Stock | Remote | ECP (same as params)
    
    $Installing = New-Object System.Collections.ArrayList

    switch ($PSCmdlet.ParameterSetName) 
    {
        'Only' {
            $WebPackages | ForEach-Object {
                if ($_['Set'] -eq $Only) {
                    if (Download-Package -URL $_['URL'] -OutFile $_['Path']) {
                        [Void]$Installing.Add($_)
                    }
                }
            }
            $Packages | ForEach-Object {
                if ($_['Set'] -eq $Only) {
                    [Void]$Installing.Add($_)
                }
            }
            break
        }
        'Except' {
            $WebPackages | ForEach-Object {
                if ($_['Set'] -ne $Except) {
                    if (Download-Package -URL $_['URL'] -OutFile $_['Path']) {
                        [Void]$Installing.Add($_)
                    }
                }
            }
            $Packages | ForEach-Object {
                if ($_['Set'] -ne $Except) {
                    [Void]$Installing.Add($_)
                }
            }
            break
        }
        default {
            $WebPackages | ForEach-Object {
                if (Download-Package -URL $_['URL'] -OutFile $_['Path']) {
                    [Void]$Installing.Add($_)
                }
            }
            $Packages | ForEach-Object {
                [Void]$Installing.Add($_)
            }
            break
        }
    } # End: Switch { }
} # End: Begin { }
Process
{
    Stage-Install $Installing
}
End
{
    switch ($PSCmdlet.ParameterSetName)
    {
        'Default' {
            Config-ECP
        }
        'Only' {
            if ($Only -eq 'ECP') {
                Config-ECP
            }
        }
        'Except' {
            if ($Except -ne 'ECP') {
                Config-ECP
            }
        }
    }
}