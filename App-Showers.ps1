[CmdletBinding()]
Param
(
    [ValidateScript({Test-Path -IsValid $_})]
    [string]$InstallDir = "C:\Configurator\"
)
Begin 
{
    . "\\hct-byn-fs01\Apps\Scripts\Functions\Misc.ps1"
    . "\\hct-byn-fs01\Apps\Scripts\Functions\File-Operations.ps1"
    . "\\hct-byn-fs01\Apps\Scripts\Functions\Process-Error.ps1"

    #Working directory
    $opsdir = "\\HCT-BYN-FS01\Documents\Heritage-Specialties\DWF"
}
#Copy necessary files to C: drive
Process
{
    Try
    {
        Write-Output "Copying SEAV program files..."
        New-Item $InstallDir -Type Directory
        Copy-Item "$opsdir\Configurator\*" $InstallDir -Recurse
    }
    Catch
    {
        Process-Error -ErrorRecord $_ -LogFile "C:\tmp\SEAV-Install-Errors.log"
    }
}
End
{
    #Create desktop shortcut for all users
    Write-Verbose "Creating desktop shortcuts..."
    New-Shortcut -Path "C:\Users\Public\Desktop\Southeastern Aluminum Vendor.lnk" -Target "$InstallDir\SoutheasternAluminumVendor.exe"

    #Move OCX file to SysWOW64 and register it
    Write-Verbose "Registering RICHTX32.ocx..."
    Register-DLL "$InstallDir\RICHTX32.ocx"

    #Set permissions such that all authenticated users can access and modify the local files
    Write-Verbose "Setting correct folder permissions on program folder."
    Set-Permission "Authenticated Users" "FullControl" $InstallDir -Cascade
}