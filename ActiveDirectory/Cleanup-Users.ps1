﻿<#
.Synopsis
Cleanup-Users will disable all Active Directory accounts (users AND computers) that have not logged in since the cutoff date.
The minimum cutoff date is precisely 1 year prior to the current date.

.Description
This script will retrieve an updated list of all Active Directory accounts that have a LastLogonDate that is earlier than the
specified cutoff. It will, by default, prompt for each account that is to be disabled, to mitigate the possibility of accidents.

Use of this script mandates the installation of the Windows Remote Management (WinRM) optional update package, which can be
obtained from Microsoft's website. In addition to this, the Active Directory Module for Windows PowerShell must be then
enabled within the Enable Optional Windows Features screen within Add/Remove Programs. In all versions of Windows that
support PowerShell, this should be easily located by entering the command "appwiz.cpl" into a PowerShell session, 
Command Prompt window, or Run dialog.

.Parameter CutoffDate
This is the oldest LastLogonDate permitted before the account will be proposed for disabling. It currently has a minimum
permitted value of exactly one year prior to the current date. It will accept a DateTime object or any string that can be
implicitly cast to DateTime.

.Parameter Exclude
Accepts an array of strings, containing SamAccountName parameters of accounts to explicitly exclude. Any string containing
an invalid SamAccountName will be ignored. Exclusion strings must include the full SamAccountName.

.Parameter Confirm
Removes all confirmation prompts and simply performs the requested actions without asking twice.

.Example
PS C:\> .\CleanupUsers.ps1 -CutoffDate (Get-Date).Years.AddYears(-2)

.Example
PS C:\> .\CleanupUsers.ps1 -CutoffDate (Get-Date).Years.AddYears(-2) -Exclude "Glen", "Cy"

.Example
PS C:\> .\CleanupUsers.ps1 -CutoffDate (Get-Date).Years.AddYears(-2) -Exclude "Glen", "Joel", "Cy", "Fabian" -Confirm
#>
[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True)]
    [ValidateScript({ 
        [DateTime]$_ -lt (Get-Date).Date.AddYears(-1) 
    })]
    [DateTime]$CutoffDate,

    [string[]]$Exclude,

    [switch]$Confirm
)
Begin 
{
    Write-Verbose "Verbose output enabled."
    Import-Module ActiveDirectory | Write-Verbose
    $UserList = Get-ADUser -Filter { LastLogonDate -lt $CutoffDate } -Properties Enabled, SamAccountName, LastLogonDate
    $FinalList = $UserList | Where-Object { ($_.SamAccountName -notin $Exclude) -and ($_.LastLogonDate -lt $CutoffDate) -and ($_.Enabled -eq $True) }
    Write-Verbose "Final user list to disable:"
    Write-Verbose $FinalList
}
Process 
{
    if ($Confirm) {
        Write-Verbose "-Confirm parameter supplied; no confirmation will be requested."
    }
    $FinalList | ForEach-Object {
        $SAN = $_.SamAccountName
        Write-Verbose "Attempting to disable account with SamAccountName '$SAN'..."
        if (
            $Confirm -or
            (Read-Host -Prompt "User '$($_.SamAccountName)' last logged on $($_.LastLogonDate). Disable Account (Y/N)") -eq "Y"
        ) {
            Disable-ADAccount $_ | Write-Verbose
            if (!(Get-ADUser -Filter {SamAccountName -eq $SAN} -Properties Enabled).Enabled) {
                Write-Host "Account successfully disabled." -ForegroundColor Green
            }
            else {
                Write-Host "Failed to disable the account; please make sure this script is being run in administrative mode." -ForegroundColor Red
            }
        }
    }
}
End 
{

}