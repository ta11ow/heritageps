﻿[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True)]
    [string]$File,
    [string]$OutFile,
    [switch]$Append
)
Begin
{
    function Get-Name {
        [CmdletBinding()]
        Param(
            [string]$Vendor,
            [string]$Number
        )
        $Request = Invoke-WebRequest -Uri "https://encrypted.google.com/search?q=$($Vendor)%20$($Number)" -UseBasicParsing -SessionVariable "session"

        # Filter block: selects only results links from a Google search page & parses them into useable URIs
            $Results = $Request.Links | Where-Object {
                $_.href -like '/url?q=http*'
            }
        
            $Target = [System.Uri]::UnescapeDataString($Results[0].href.Split('=')[1].Split('&')[0])
        # Filter block: end

        $Request = Invoke-WebRequest -Uri $Target -SessionVariable 'session'
        $Title = $Request.ParsedHtml.title

        Write-Output $Title
    }

    function Import-Data {
        [CmdletBinding()]
        Param(
            [string]$Path
        )
        try {
            $Data = Import-Csv -Path $Path -Delimiter ','
        }
        catch [System.IO.IOException] {
            Write-Error "An exception was encountered trying to load the file from `"$($_.Exception.FilePath)`""
            Write-Error $_.Exception
            Write-Error $_.Exception.Message
        }
        Write-Output $Data
    }

    
    if (!$PSBoundParameters.ContainsKey('File')) {
        $File = Read-Host -Prompt "Enter the CSV file path to parse"
    }
    $Data = Import-Data -Path $File
    $NameList = New-Object System.Collections.ArrayList
}
Process
{
    $Data | ForEach-Object {  
        [void]$NameList.Add(@{
            PartNumber = $_.PartNumber
            Description = Get-Name -Vendor $_.Vendor -Number $_.PartNumber
        })
    }
}
End
{
    try {
        $NameList | Export-Csv -Delimiter ',' -Path $OutFile -Force
    }
    catch {
            Write-Error "An exception was encountered trying to write the file to `"$($_.Exception.FilePath)`""
            Write-Error $_.Exception
            Write-Error $_.Exception.Message
    }
}