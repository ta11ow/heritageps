﻿[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true)]
    [ValidateScript({Test-Path $_})]
    $Path,
         
    [ValidateSet('Center','Stretch','Fill','Tile','Fit')]
    $Style
)
Begin 
{
    function Update-Wallpaper 
    {
        [CmdletBinding()]
        Param(
            [Parameter(Mandatory=$true)]
            $Path,
         
            [ValidateSet('Center','Stretch','Fill','Tile','Fit')]
            $Style
        )
        Add-Type -TypeDefinition @"
            using System;
            using System.Runtime.InteropServices;
            namespace WMI {
                public class Broadcast
                {
                    [DllImport("user32.dll", SetLastError = true)]
                    private static extern IntPtr SendMessageTimeout(IntPtr hWnd, int Msg, IntPtr wParam, string lParam, uint fuFlags, uint uTimeout, IntPtr lpdwResult);

                    private static readonly IntPtr HWND_BROADCAST = new IntPtr(0xffff);
                    private const int WM_SETTINGCHANGE = 0x1a;
                    private const int SMTO_ABORTIFHUNG = 0x0002;

                    public static void SettingsChange()
                    {
                        SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, IntPtr.Zero, null, SMTO_ABORTIFHUNG, 100, IntPtr.Zero);
                    }
                }
            }
"@
        if (Test-Path "$($env:APPDATA)\Microsoft\Windows\Themes\CachedFiles") {
            Remove-Item -Path "$($env:APPDATA)\Microsoft\Windows\Themes\CachedFiles" -Force
        }
        Remove-Item -Path "$($env:APPDATA)\Microsoft\Windows\Themes\*" -Force 
        Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop\' -Name Wallpaper -Value $Path
        Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Internet Explorer\Desktop\General' -Name WallpaperSource -Value $Path
        $params = @{
            FilePath = "rundll32.exe" 
            NoNewWindow = $True
            Wait = $True
            PassThru = $True
            ArgumentList = "user32.dll,UpdatePerUserSystemParameters 1, True"
        }
    
        $results = New-Object System.Collections.ArrayList

        for ($i = 0; $i -lt 10; $i++) {
            $results.Add((Start-Process @params)) | Out-Null
        }
        $results | ForEach-Object { Write-Output $_ }

        [WMI.Broadcast]::SettingsChange()

        Stop-Process -Name explorer
    }
}
Process 
{  
    Write-Output (Update-Wallpaper $Path $Style)
}