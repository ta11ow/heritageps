﻿[CmdletBinding()]
Param(
    [System.Uri]$OriginalUri,
    [Int32]$MaxConnections = 5,
    [Int32]$Limit = 50
)
Begin
{
    if (!$PSBoundParameters.ContainsKey('OriginalUri')) {
        try {
            [System.Uri] $OriginalUri = (Read-Host -Prompt "Provide a starting URI for the crawler")
        }
        catch {
            Write-Error "Could not parse the URL into usable data. Blame [System.Uri], not me."
        }
        Write-Output "Using:`n$OriginalUri"
    }
    $Operation = [ScriptBlock]::Create({
        Param(
            [System.Uri]$Uri
        )
        Begin {
            $uri_list = New-Object System.Collections.Generic.List[System.Uri]
        }
        Process {
            $urls = (Invoke-WebRequest -TimeoutSec 5 -Uri $url -Method Get -UseBasicParsing -SessionVariable session -ErrorAction Ignore -MaximumRedirection 5).Links 
            foreach ($url in $urls) {
                [void]$uri_list.Add([System.Uri] $url)
            }
        }
        End {
            Write-Output $uri_list
        }
    })

    $UnvisitedURIs = New-Object System.Collections.Generic.List[System.Uri]
    $VisitedURIs = New-Object System.Collections.Generic.List[System.Uri]
    $Jobs = New-Object System.Collections.Generic.List[System.Collections.Hashtable]

    #region RunspacePool
        [RunspaceFactory]::CreateRunspacePool()
        $SessionState = [System.Management.Automation.Runspaces.InitialSessionState]::CreateDefault()
        $RunspacePool = [RunspaceFactory]::CreateRunspacePool(1,$MaxConnections)
        $RunspacePool.Open()
    #endregion
}
Process
{
    foreach ($loop in 1..50) {
        if ($loop -eq 1) {
            $Parameters = @{
                Uri = $OriginalUri
            }
            $Instance = [PowerShell]::Create()
            $Instance.RunspacePool = $RunspacePool
            [void]$Instance.AddScript($Operation)
            [void]$Instance.AddParameters($Parameters)

            $output = [PSObject] $Instance.Invoke()
            
            foreach ($Object in $output) {
                if (!$UnvisitedURIs.Contains($Object) -and !$VisitedURIs.Contains($Object)) {
                    [void]$UnvisitedURIs.Add($Object)
                }
            }
            Start-sleep -Seconds 10
        }
        else
        {
            $target = $UnvisitedURIs[(Get-Random -Minimum 0 -Maximum ($UnvisitedURIs.Count))]
            [void]$UnvisitedURIs.Remove($target)
            [void]$VisitedURIs.Add($target)
            $Parameters = @{
                Uri = $new
            }
        

            $Instance = [PowerShell]::Create()
            $Instance.RunspacePool = $RunspacePool
            [void]$Instance.AddScript($Operation)
            [void]$Instance.AddParameters($Parameters)

            $NewJob = @{
                Instance = $Instance
                AsyncObject = [PSObject] $Instance.BeginInvoke()
            }
            [void]$Jobs.Add($NewJob)

            foreach ($Task in $Jobs) {
                if ($Task.AsyncObject.IsCompleted) {
                    $Output = $Task.Instance.EndInvoke($Task.AsyncObject)
                    $Task.Instance.Dispose()
                    $Jobs.Remove($Task)
                    #We're expecting output to be in the form of a List[System.Uri]
                    foreach ($Object in $Output) {
                        if (!$UnvisitedURIs.Contains($Object) -and !$VisitedURIs.Contains($Object)) {
                            [void]$UnvisitedURIs.Add($Object)
                        }
                    }
                }
            } # foreach (jobs)
        } # else
    } #foreach (loop)
}
End
{

}