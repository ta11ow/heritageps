﻿[CmdletBinding()]
Param()
Begin 
{
    $dataFolder = "\\hct-byn-fs01\apps\scripts\POCs\Data"

    $url = [System.Uri] (Read-Host -Prompt "Provide a seed URL")
    $num = [int] (Read-Host -Prompt "Enter a number of requests")

    $LinkList = New-Object System.Collections.Generic.List[System.Uri]
    $VisitedList = New-Object System.Collections.Generic.List[System.Uri]
}
Process 
{
    foreach ($loop in 1..$num)
    {
        $VisitedList.Add($url) | Out-Null
        $urls = (Invoke-WebRequest -TimeoutSec 5 -Uri $url -Method Get -UseBasicParsing -SessionVariable session -ErrorAction Ignore -MaximumRedirection 5).Links 
        foreach ($s in $urls) {
            $uri = [System.Uri] $s.href
            if (!$LinkList.Contains($uri) -and ($uri).IsAbsoluteUri -and (!$VisitedList.Contains($uri))) {
                $LinkList.Add($new) | Out-Null
                Write-Output $new.AbsoluteUri
            }
        }

        $index = Get-Random -Minimum 0 -Maximum ($LinkList.Count - 1)
    
        $url = $LinkList[$index].AbsoluteUri
        $LinkList.RemoveAt($index)

        Write-Output "`nURI list size: $($LinkList.Count)"
        Write-Output "New URL target #$($loop + 1): $url"
    }
}
End 
{
    if(!(Test-Path $dataFolder)) {
    New-Item -ItemType directory -Path $dataFolder
    }


    Write-Output "Compiling lists..."
    $LinkList.AddRange($VisitedList) | Out-Null

    Write-Output "Writing list to file..."
    Export-Clixml -Path ("$datafolder\CrawlerLinks_$((Get-Date).ToString('yyyyMMdd')).xml") -InputObject $LinkList
}