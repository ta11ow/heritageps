﻿[CmdletBinding()]
Param(
    [Parameter(ValueFromPipeline=$True)]
    [ScriptBlock]$Operation,
    [Array]$Parameters,
    [Int32]$MaxConcurrent = 5
)
Begin
{
    #region RunspacePool
        [RunspaceFactory]::CreateRunspacePool()
        $SessionState = [System.Management.Automation.Runspaces.InitialSessionState]::CreateDefault()
        $RunspacePool = [RunspaceFactory]::CreateRunspacePool(1,$MaxConcurrent)
        $Instance = [PowerShell]::Create()
        $Instance.RunspacePool = $RunspacePool
        $RunspacePool.Open()
    #endregion

    [void]$Instance.AddScript($Operation)
    [void]$Instance.AddParameters($Parameters)
}
Process
{
    $AsyncHandler = $Instance.Invoke()
    Write-Host $AsyncHandler
    while (!$AsyncHandler.IsCompleted) {
        Write-Output "The process is running."
        Start-Sleep 1
    }
}
End
{
    Write-Output "The process has completed."
    Write-Output $Instance.EndInvoke($AsyncHandler)
    $Instance.Dispose()
}